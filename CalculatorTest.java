

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class CalculatorTest.
 *
 * Daniel Borquaye
 */
public class CalculatorTest
{
    /**
     * Default constructor for test class CalculatorTest
     */
    public CalculatorTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void number()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(1);
        calculat1.number(2);
        calculat1.number(0);
        calculat1.number(4);
        assertEquals("1204.0", calculat1.currentDisplay());
    }

    @Test
    public void plusMinus()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(1);
        calculat1.plusMinus();
        calculat1.plusMinus();
        assertEquals("1.0", calculat1.currentDisplay());
    }

    @Test
    public void sqrt()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(1);
        calculat1.number(6);
        calculat1.sqrt();
        assertEquals("4.0", calculat1.currentDisplay());
    }

    @Test
    public void add()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(1);
        calculat1.add();
        calculat1.number(6);
        calculat1.equate();
        calculat1.add();
        calculat1.number(3);
        calculat1.number(2);
        calculat1.equate();
        assertEquals("39.0", calculat1.currentDisplay());
    }

    @Test
    public void subtract()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(5);
        calculat1.subtract();
        calculat1.number(2);
        calculat1.equate();
        assertEquals("3.0", calculat1.currentDisplay());
    }

    @Test
    public void multiply()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(5);
        calculat1.multiply();
        calculat1.number(2);
        calculat1.equate();
        assertEquals("10.0", calculat1.currentDisplay());
    }

    @Test
    public void divide()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(1);
        calculat1.number(0);
        calculat1.divide();
        calculat1.number(2);
        calculat1.equate();
        assertEquals("5.0", calculat1.currentDisplay());
    }

    @Test
    public void memStore()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(4);
        calculat1.memStore();
        calculat1.number(2);
        assertEquals(4.0, calculat1.getStored(), 0.1);
    }

    @Test
    public void memRecall()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(4);
        calculat1.memStore();
        calculat1.number(2);
        calculat1.memRecall();
        assertEquals("4.0", calculat1.currentDisplay());
    }

    @Test
    public void memClear()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(4);
        calculat1.memStore();
        calculat1.number(2);
        calculat1.memClear();
        assertEquals(0.0, calculat1.getStored(), 0.1);
    }

    @Test
    public void dot()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(1);
        calculat1.dot();
        calculat1.number(1);
        calculat1.number(1);
        calculat1.add();
        calculat1.number(5);
        calculat1.dot();
        calculat1.number(0);
        calculat1.number(1);
        calculat1.equate();
        assertEquals("6.12", calculat1.currentDisplay());
    }

    @Test
    public void delete()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(1);
        calculat1.number(2);
        calculat1.number(3);
        calculat1.delete();
        calculat1.delete();
        assertEquals("1.0", calculat1.currentDisplay());
    }

    @Test
    public void equate()
    {
        Calculator calculat1 = new Calculator();
        calculat1.number(2);
        calculat1.number(0);
        calculat1.subtract();
        calculat1.number(1);
        calculat1.number(0);
        calculat1.equate();
        calculat1.divide();
        calculat1.number(2);
        calculat1.equate();
        calculat1.multiply();
        calculat1.number(3);
        calculat1.equate();
        calculat1.add();
        calculat1.number(1);
        calculat1.dot();
        calculat1.number(1);
        calculat1.equate();
        assertEquals("16.1", calculat1.currentDisplay());
    }
}













