/**
 * Daniel Borquaye - DB363
 */
import static java.lang.Math.*;
public class Calculator
{
  private double previousNum;
  private double currentNum;
  private double storedNum;
  private boolean store;
  private boolean add;
  private boolean add_;
  private boolean subtract;
  private boolean subtract_;
  private boolean multiply;
  private boolean multiply_;
  private boolean divide;
  private boolean divide_;
  private boolean dot;
  private double k = 0.1;
  private double c = 10;
  /**
   * Constructor
   */
  public Calculator()
  {
  }
  
  /**
   * Method to reset scaling values
   */
  public void clear() 
  {
      k = 0.1;
      c = 10;
      dot = false;
  }
  
  /**
   * Method to reset scaling values
   */
  public void reset() 
  {
      currentNum = 0;
  }
  
  /**
   * Set the previous number variable to whatever the current 
   * number is
   */
  public void divide() 
  {
      clear();
      divide = true;
      divide_ = true;
      previousNum = currentNum;
  }
  
  /**
   * 
   */
  public void sqrt() 
  {
      currentNum = Math.sqrt(currentNum);
      /*currentDisplay();*/
  }
    
  /**
   * Set the previous number variable to whatever the current
   * number is
   */
  public void multiply() 
  {
      clear();
      multiply = true;
      multiply_ = true;
      previousNum = currentNum;
  }
  
  /**
   * Set the previous number variable to whatever the current
   * number is
   */
  public void subtract() 
  {
      clear();
      subtract = true;
      subtract_ = true;
      previousNum = currentNum;
  }
  
  /**
   * 
   */
  public void plusMinus() 
  {
     currentNum = currentNum*(-1);
     currentDisplay();
  }
  
  /**
   * 
   */
  public void dot() 
  {
      dot = true;
  }
  
  /**
   * Set the previous number variable to whatever the current
   * number is
   */
  public void add() 
  {
      clear();
      add = true;
      add_=true;
      previousNum = currentNum;
  }
  
  /**
   * Method for when the equals button is pressed
   */
  public void equate() 
  {
      if (add_ == true){
        currentNum = currentNum + previousNum;
        add_ = false;
      }
      else if (subtract_ == true){
        currentNum = previousNum - currentNum;
        subtract_ = false;
      }
      else if (multiply_ == true){
        currentNum = previousNum*currentNum;
        multiply_ = false;
      }
      else if (divide_ == true){
        currentNum = previousNum/currentNum;
        divide_ = false;
      }
      /*currentDisplay();*/
  }
  
  /**
   * 
   */
  public void number(int n) 
  { 
      if (add == true || subtract == true || multiply == true || divide == true) {
          currentNum = 0;
          currentNum = (currentNum*10)+n;
          add=false;
          subtract=false;
          multiply=false;
          divide=false;
      }
      else if (dot == true){
          currentNum = ((((currentNum*c)+n)*k));
          c = c * 10;
          k = k/10;
      }
      else if (store==true) {
          currentNum = 0;
          currentNum = (currentNum*10)+n;
          store = false;
      }
      else if (n == 0) {
          currentNum = currentNum*10;
      }
      else {
          currentNum = (currentNum*10)+n;
      }
      /*currentDisplay();*/
  }
  
  /**
   * delete the right most number 
   * (rounding down towards negative infinity Math.floor)
   */
  public void delete() 
  {
      currentNum = Math.floor(currentNum/10);
      /*currentDisplay();*/
  }
  
  /**
   * Store the current number in the stored number field
   */
  public void memStore() 
  {
      store = true;
      storedNum = currentNum;
  }
  
  /**
   * Set the current number to the number stored in the stored
   * number field
   */
  public void memRecall() 
  {
      currentNum = storedNum;
      /*currentDisplay();*/
  }
  
  /**
   * Set the number stored in the stored number field to 0
   */
  public void memClear() 
  {
      storedNum = 0;
  }
  
  public double getStored()
  {
      return storedNum;
  }
  
  /**
   * Display the current number
   */
  public String currentDisplay() 
  { 
      return ""+currentNum; 
  }
}